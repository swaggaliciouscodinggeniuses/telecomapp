package com.swaglords.cse110app.Account;


import java.io.Serializable;

public abstract class Account implements Serializable
{
	public enum AccountType { CUSTOMER_ACCOUNT, CUSTOMER_SERVICE_ACCOUNT }
	public enum AccountPackage { BASIC_PACKAGE, PLUS_PACKAGE, MLG_PACKAGE }

	private String username;
	private String password;
	private String email;
	private String firstName;
	private String lastName;
	private String address;
	private AccountType accountType;

	Account()
	{
		username = "";
		accountType = AccountType.CUSTOMER_ACCOUNT;
	}

	public abstract void addInternet(String internet);
	public abstract void addCable(String cable);
	public abstract void addPhone(String phone);

	public String getUsername()
	{
		return username;
	}
	public void setUsername(String username)
	{
		this.username = username;
	}

	public String getPassword()
	{
		return password;
	}
	public void setPassword(String password)
	{
		this.password = password;
	}

	public AccountType getAccountType ()
	{
		return accountType;
	}
	public void setAccountType(AccountType accountType)
	{
		this.accountType = accountType;
	}

	public String getEmail()
	{
		return email;
	}
	public void setEmail(String email)
	{
		this.email = email;
	}

	public String getFirstName()
	{
		return firstName;
	}
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}

	public String getLastName()
	{
		return lastName;
	}
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}

	public String getAddress()
	{
		return address;
	}
	public void setAddress(String address)
	{
		this.address = address;
	}
}

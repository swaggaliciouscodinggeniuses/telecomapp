package com.swaglords.cse110app.Account;

import com.parse.ParseObject;

import java.util.ArrayList;

public class CustomerAccount extends Account
{
	private ArrayList<AccountPackage> packages;
	private String internet;
	private String cable;
	private String phone;

	public CustomerAccount ()
	{

		internet = InternetService.NO_INTERNET;
		cable = CableService.NO_CABLE;
		phone = PhoneService.NO_PHONE;

	}

	public void addInternet(String internet)
	{
		this.internet = internet;

	}

	public void addCable(String cable)
	{
		this.cable = cable;

	}

	public void addPhone(String phone)
	{
		this.phone = phone;

	}

    public void updateServices(String internet, String cable, String phone){
        addInternet(internet);
        addCable(cable);
        addPhone(phone);
    }

	public void displayInfo ()
	{

	}

	public void viewBill (){
        // check packages

        // if no packages, check each service

        // bill = sum of all their service prices
	}

	public ArrayList<AccountPackage> getPackages()
	{
		return packages;
	}

	public void setPackages(ArrayList<AccountPackage> packages)
	{
		this.packages = packages;
	}

	public String getInternet()
	{
		return internet;
	}

	public void setInternet(String internet)
	{
		this.internet = internet;
	}

	public String getCable()
	{
		return cable;
	}

	public void setCable(String cable)
	{
		this.cable = cable;
	}

	public String getPhone()
	{
		return phone;
	}

	public void setPhone(String phone)
	{
		this.phone = phone;
	}
}

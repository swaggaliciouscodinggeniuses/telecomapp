package com.swaglords.cse110app.Login;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.parse.ParseObject;
import com.swaglords.cse110app.Account.CustomerAccount;
import com.swaglords.cse110app.R;

import java.util.List;

public class LoginActivity extends ActionBarActivity {
	private Context appContext;
	private DisplayMetrics displayInfo;
	private EditText username, password;
	private Button login;
    private final String admin = "swagmaster";
    private final String adminPassword = "swaglord";
    private final String marketingRep = "mrep";
    private final String marketingRepPassword = "mreppw";

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up_or_login);

		appContext = LoginActivity.this;
		displayInfo = getResources().getDisplayMetrics();

		//Initialize elements
		username = (EditText) findViewById(R.id.signInUserName);
		password = (EditText) findViewById(R.id.signInPassword);
		login = (Button) findViewById(R.id.loginButton);

        //Create a customer account to hold user data from database
        final CustomerAccount customer = new CustomerAccount();

		login.setOnClickListener(new View.OnClickListener() {
			@Override
			public void  onClick (View view) {

                if(username.getText().toString().toLowerCase().equals(admin)
                        && password.getText().toString().equals(adminPassword)){
                    Intent intent = new Intent(appContext, CustomerServiceAccountActivity.class);
					username.setText("");
					password.setText("");

                    startActivity(intent);
                }
                else if(username.getText().toString().toLowerCase().equals(marketingRep)
                        && password.getText().toString().equals(marketingRepPassword)) {
                    Intent intent = new Intent(appContext, MarketingRepAccountActivity.class);
                    username.setText("");
                    password.setText("");

                    startActivity(intent);
                }
                else{
                    // Show a progress dialog
                    final AlertDialog.Builder progressBuilder = new AlertDialog.Builder(appContext);
                    progressBuilder.setTitle("Logging in...");
                    int padding = (int) (20 * displayInfo.density);
                    ProgressBar bar = new ProgressBar(appContext);
                    bar.setPadding(0, padding, 0, padding);
                    progressBuilder.setView(bar);

                    final AlertDialog progressDialog = progressBuilder.create();
                    progressDialog.show();

                    //Start logging into the Parse database
                    ParseUser.logInInBackground(username.getText().toString().toLowerCase(), password.getText().toString()
                            , new LogInCallback()
                    {
                        @Override
                        public void done(ParseUser parseUser, ParseException e)
                        {
                            // Get rid of the login dialog
                            progressDialog.dismiss();

                            if (e != null) {
                                // Prepare an alert dialog to notify the user of an error
                                final AlertDialog.Builder loginFailedDialog = new AlertDialog.Builder(appContext);
                                loginFailedDialog.setTitle(R.string.login_failed);
                                loginFailedDialog.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener()
                                {
                                    public void onClick(DialogInterface dialog, int which)
                                    {
                                    }
                                });

                                // Show the dialog with the appropriate message
                                if (isEmpty(username) || isEmpty(password)) {
                                    if (!isEmpty(password)) // If the password is filled, the username isn't
                                        loginFailedDialog.setMessage(R.string.login_failed_message_no_username);
                                    else if (!isEmpty(username)) // If the username is filled, the password isn't
                                        loginFailedDialog.setMessage(R.string.login_failed_message_no_password);
                                    else // Otherwise neither are filled
                                        loginFailedDialog.setMessage(R.string.login_failed_message_no_input);

                                    loginFailedDialog.show();
                                }
                                else {
                                    loginFailedDialog.setMessage(R.string.login_failed_message);
                                    loginFailedDialog.show();
                                }
                            }
                            else {
                                // Start HomeActivity
                                Intent intent = new Intent(appContext, CustomerAccountActivity.class);
								username.setText("");
								password.setText("");

                                // account.setAccountType(Account.AccountType.CUSTOMER_ACCOUNT);
                                customer.setUsername(parseUser.getUsername());
                                customer.setEmail((String) parseUser.getEmail());
                                customer.setFirstName((String) parseUser.get("firstName"));
                                customer.setLastName((String) parseUser.get("lastName"));
                                customer.setAddress((String) parseUser.get("address"));

                                /* is this how it works?
                                ParseQuery query = new ParseQuery("UserServices");
                                query.getInBackground("kmjwjUqOH8", new GetCallback<ParseObject>() {
                                    public void done(ParseObject object, ParseException e) {
                                        customer.setInternet(object.getString("internet"));
                                        customer.setCable(object.getString("cable"));
                                        customer.setPhone(object.getString("phone"));
                                    }
                                });*/

                                // use UserServices
                                ParseQuery<ParseObject> query = ParseQuery.getQuery("UserServices");
                                query.whereEqualTo("username", parseUser.getUsername());
                                query.findInBackground(new FindCallback<ParseObject>() {
                                    public void done(List<ParseObject> userServiceList, ParseException e) {
                                        if (e == null) {
                                            //Log.d("score", "Retrieved " + scoreList.size() + " scores");
                                            customer.setCable(userServiceList.get(0).getString("cable"));
                                            customer.setInternet(userServiceList.get(0).getString("internet"));
                                            customer.setPhone(userServiceList.get(0).getString("phone"));
                                        } else {
                                            //Log.d("score", "Error: " + e.getMessage());
                                        }
                                    }
                                });

                                //ParseQueryAdapter<ParseObject> adapter = new ParseQueryAdapter<ParseObject>(this, factory);

                                // need to get these from UserServices
                                //customer.setInternet((String) parseUser.get(("internet")));
                                //customer.setCable((String) parseUser.get(("cable")));
                                //customer.setPhone((String) parseUser.get(("phone")));
                                //  account.setPackages((ArrayList<Account.AccountPackage>) parseUser.get("packages"));

                                intent.putExtra("currentCustomer", customer);

                                startActivity(intent);
                            }
                        }
                    });
                }
			}
		});
    }


	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.menu_sign_up_or_login, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();

		//noinspection SimplifiableIfStatement
		if (id == R.id.action_settings) {
			return true;
		}

		return super.onOptionsItemSelected(item);
	}

	private boolean isEmpty(EditText editText) {
		return (editText.getText().toString().trim().length() <= 0);
	}
}

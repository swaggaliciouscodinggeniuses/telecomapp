package com.swaglords.cse110app.Login;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.swaglords.cse110app.Account.CableService;
import com.swaglords.cse110app.Account.CustomerAccount;
import com.swaglords.cse110app.Account.InternetService;
import com.swaglords.cse110app.Account.PhoneService;
import com.swaglords.cse110app.R;

import java.util.ArrayList;
import java.util.List;

public class MarketingRepAccountCableActivity extends ActionBarActivity {
    private ArrayList<String> services;
    private ListView cableList;
    private EditText serviceName;
    private EditText servicePrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marketing_rep_account_cable_services);
        cableList = (ListView)findViewById(R.id.cable_list);

        ParseQueryAdapter.QueryFactory<ParseObject> factory =
                new ParseQueryAdapter.QueryFactory<ParseObject>() {
                    public ParseQuery create() {
                        ParseQuery query = new ParseQuery("Services");
                        query.whereEqualTo("type", "CableService");
                       // query.orderByDescending("moneySpent");
                        return query;
                    }
                };

        ParseQueryAdapter<ParseObject> adapter = new ParseQueryAdapter<ParseObject>(this, factory);
        adapter.setTextKey("name");
        cableList.setAdapter(adapter);

        findViewById(R.id.update_services).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                serviceName = (EditText) findViewById(R.id.service_text);
                servicePrice = (EditText) findViewById(R.id.price_text);
                ParseObject servicez = new ParseObject("Services");
                servicez.put("name", serviceName.getText().toString());
                servicez.put("price", Integer.parseInt(servicePrice.getText().toString()));
                servicez.put("type", "CableService");
                servicez.saveEventually();
            }
        });
    }



        /*ParseQuery<ParseObject> query = ParseQuery.getQuery("Services");
        query.whereEqualTo("type", "CableService");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> parseCableList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + parseCableList.size() + " scores");
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        }); */

        //ParseQueryAdapter<ParseObject> adapter = new ParseQueryAdapter<ParseObject>();

        //cableList.setAdapter();

        //Intent intent = getIntent();
        //final CustomerAccount customer = (CustomerAccount)intent.getSerializableExtra("currentCustomer");

        /*cableList = (ListView) findViewById(R.id.cable_list);


        String[] cableStrings = getResources().getStringArray(R.array.cable_services);
        for (int i = 0; i < cableStrings.length; i++)
        {
            if (cableStrings[i].equals(customer.getCable()))
            {
                cableList.setSelection(i);
            }
        }*/


        /*findViewById(R.id.updateServicesButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String internetService;
                String cableService;
                String phoneService;

                String temp = cableSpinner.getSelectedItem().toString();

                //Get cable service
                if(cableSpinner.getSelectedItem().toString().equals(getString(R.string.basic_cable))){
                    cableService = CableService.BASIC_CABLE;
                }
                else if(cableSpinner.getSelectedItem().toString().equals(getString(R.string.fox_news_only))){
                    cableService = CableService.FOX_NEWS_ONLY;
                }
                else{
                    cableService = CableService.NO_CABLE;
                }

                //Get internet
                if(internetSpinner.getSelectedItem().toString().equals(getString(R.string.basic_internet))){
                    internetService = InternetService.BASIC_INTERNET;
                }
                else if(internetSpinner.getSelectedItem().toString().equals(getString(R.string.korean_internet))){
                    internetService = InternetService.KOREAN_INTERNET;
                }
                else{
                    internetService = InternetService.NO_INTERNET;
                }

                //Get phone
                if(phoneSpinner.getSelectedItem().toString().equals(getString(R.string.basic_phone))){
                    phoneService = PhoneService.BASIC_PHONE;
                }
                else if(phoneSpinner.getSelectedItem().toString().equals(getString(R.string.spongebob_unlimited))){
                    phoneService = PhoneService.SPONGEBOB_UNLIMITED;
                }
                else{
                    phoneService = PhoneService.NO_PHONE;
                }

                customer.updateServices(internetService, cableService, phoneService);
                ParseQuery query = ParseUser.getQuery();
                query.whereEqualTo("username", customer.getUsername());
                query.findInBackground(new FindCallback<ParseUser>()
                {
                    @Override
                    public void done(List<ParseUser> parseUsers, ParseException e)
                    {
                        if (e == null)
                        {
                            ParseUser parseUser = parseUsers.get(0);
                            parseUser.put("internet", customer.getInternet());
                            parseUser.put("cable", customer.getCable());
                            parseUser.put("phone", customer.getPhone());

                            parseUser.saveInBackground(new SaveCallback()
                            {
                                @Override
                                public void done(ParseException e)
                                {
                                    if (e == null)
                                    {
                                        Toast.makeText(CustomerAccountActivity.this, getString(R.string.services_updated), Toast.LENGTH_LONG).show();
                                    }
                                    else
                                    {
                                        Toast.makeText(CustomerAccountActivity.this, "Unable to edit user information", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        }
                        else
                        {
                            Toast.makeText(CustomerAccountActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }*/


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_customer_account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}


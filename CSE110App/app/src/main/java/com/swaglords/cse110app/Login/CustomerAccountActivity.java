package com.swaglords.cse110app.Login;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Spinner;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.swaglords.cse110app.Account.CableService;
import com.swaglords.cse110app.Account.CustomerAccount;
import com.swaglords.cse110app.Account.InternetService;
import com.swaglords.cse110app.Account.PhoneService;
import com.swaglords.cse110app.R;

import java.util.ArrayList;
import java.util.List;

public class CustomerAccountActivity extends ActionBarActivity {
    private ArrayList<String> services;
    private Spinner cableSpinner;
    private Spinner internetSpinner;
    private Spinner phoneSpinner;
    private String cable;
    private String internet;
    private String phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_account);

        findViewById(R.id.viewBillButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent =
                        new Intent(CustomerAccountActivity.this, BillingActivity.class);
                startActivity(intent);
            }
        });

		Intent intent = getIntent();
		final CustomerAccount customer = (CustomerAccount)intent.getSerializableExtra("currentCustomer");

        cableSpinner = (Spinner) findViewById(R.id.cableDropdown);
        internetSpinner = (Spinner) findViewById(R.id.internetDropdown);
        phoneSpinner = (Spinner) findViewById(R.id.phoneDropdown);


        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserServices");
        query.whereEqualTo("username", customer.getUsername());
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> userServiceList, ParseException e) {
                if (e == null) {
                    //Log.d("score", "Retrieved " + scoreList.size() + " scores");
                    cable = (userServiceList.get(0).getString("cable"));
                    internet = (userServiceList.get(0).getString("internet"));
                    phone = (userServiceList.get(0).getString("phone"));

                } else {
                    //Log.d("score", "Error: " + e.getMessage());
                }
            }
        });


        if(cable == CableService.NO_CABLE){
            cableSpinner.setSelection(0);
        } else if (cable == CableService.BASIC_CABLE){
            cableSpinner.setSelection(1);
        } else {
            cableSpinner.setSelection(2);
        }

        if(internet == InternetService.NO_INTERNET){
            internetSpinner.setSelection(0);
        } else if (internet == InternetService.BASIC_INTERNET){
            internetSpinner.setSelection(1);
        } else {
            internetSpinner.setSelection(2);
        }

        if(phone == PhoneService.NO_PHONE){
            phoneSpinner.setSelection(0);
        } else if (cable == PhoneService.BASIC_PHONE){
            phoneSpinner.setSelection(1);
        } else {
            phoneSpinner.setSelection(2);
        }
        /*
        String[] cableStrings = getResources().getStringArray(R.array.cable_services);
		for (int i = 0; i < cableStrings.length; i++)
		{
			if (cableStrings[i].equals(cable))
			{
				cableSpinner.setSelection(i);
			}
		}

		String[] internetStrings = getResources().getStringArray(R.array.internet_services);
		for (int i = 0; i < internetStrings.length; i++)
		{
			if (internetStrings[i].equals(internet))
			{
				internetSpinner.setSelection(i);
			}
		}

		String[] phoneStrings = getResources().getStringArray(R.array.phone_services);
		for (int i = 0; i < phoneStrings.length; i++)
		{
			if (phoneStrings[i].equals(phone))
			{
				phoneSpinner.setSelection(i);
			}
		}*/


        findViewById(R.id.updateServicesButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String internetService;
                String cableService;
                String phoneService;

				String temp = cableSpinner.getSelectedItem().toString();



                //Get cable service
                if(cableSpinner.getSelectedItem().toString().equals(getString(R.string.basic_cable))){
                    cableService = CableService.BASIC_CABLE;
                }
                else if(cableSpinner.getSelectedItem().toString().equals(getString(R.string.fox_news_only))){
                    cableService = CableService.FOX_NEWS_ONLY;
                }
                else{
                    cableService = CableService.NO_CABLE;
                }

                //Get internet
                if(internetSpinner.getSelectedItem().toString().equals(getString(R.string.basic_internet))){
                    internetService = InternetService.BASIC_INTERNET;
                }
                else if(internetSpinner.getSelectedItem().toString().equals(getString(R.string.korean_internet))){
                    internetService = InternetService.KOREAN_INTERNET;
                }
                else{
                    internetService = InternetService.NO_INTERNET;
                }

                //Get phone
                if(phoneSpinner.getSelectedItem().toString().equals(getString(R.string.basic_phone))){
                    phoneService = PhoneService.BASIC_PHONE;
                }
                else if(phoneSpinner.getSelectedItem().toString().equals(getString(R.string.spongebob_unlimited))){
                    phoneService = PhoneService.SPONGEBOB_UNLIMITED;
                }
                else{
                    phoneService = PhoneService.NO_PHONE;
                }


                // query from UserServices instead of ParseUser for Customer
                customer.updateServices(internetService, cableService, phoneService);
				ParseQuery query = ParseUser.getQuery();
				query.whereEqualTo("username", customer.getUsername());
				query.findInBackground(new FindCallback<ParseUser>()
				{
					@Override
					public void done(List<ParseUser> parseUsers, ParseException e)
					{
						if (e == null)
						{
							//ParseUser parseUser = parseUsers.get(0);

                            // update services into the UserServices fields
                            ParseQuery<ParseObject> query = ParseQuery.getQuery("UserServices");
                            query.whereEqualTo("username", customer.getUsername());
                            query.findInBackground(new FindCallback<ParseObject>() {
                                public void done(List<ParseObject> userServiceList, ParseException e) {
                                    if (e == null) {
                                        //Log.d("score", "Retrieved " + scoreList.size() + " scores");
                                        userServiceList.get(0).put("internet", customer.getInternet());
                                        userServiceList.get(0).put("cable", customer.getCable());
                                        userServiceList.get(0).put("phone", customer.getPhone());
                                        userServiceList.get(0).saveInBackground();
                                        Toast.makeText(CustomerAccountActivity.this, "Updated Customer Services",
                                                Toast.LENGTH_LONG).show();
                                    } else {
                                        Toast.makeText(CustomerAccountActivity.this, "Something went wrong",
                                                Toast.LENGTH_LONG).show();
                                    }
                                }
                            });

							//parseUser.put("internet", customer.getInternet());
							//parseUser.put("cable", customer.getCable());
							//parseUser.put("phone", customer.getPhone());



							/*parseUser.saveInBackground(new SaveCallback()
							{
								@Override
								public void done(ParseException e)
								{
									if (e == null)
									{
										Toast.makeText(CustomerAccountActivity.this, getString(R.string.services_updated), Toast.LENGTH_LONG).show();
									}
									else
									{
										Toast.makeText(CustomerAccountActivity.this, "Unable to edit user information", Toast.LENGTH_LONG).show();
									}
								}
							}); */
						}
						else
						{
							Toast.makeText(CustomerAccountActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
						}
					}
				});
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_customer_account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}

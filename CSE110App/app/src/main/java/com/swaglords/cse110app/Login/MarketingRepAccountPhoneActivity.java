package com.swaglords.cse110app.Login;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ListView;

import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseQueryAdapter;
import com.swaglords.cse110app.R;

/**
 * Created by Tyler on 2/25/2015.
 */
public class MarketingRepAccountPhoneActivity extends ActionBarActivity {

    private ListView phoneList;

    private EditText phoneName;
    private EditText phonePrice;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_marketing_rep_account_phone_services);
        phoneList = (ListView)findViewById(R.id.phone_list);

        ParseQueryAdapter.QueryFactory<ParseObject> factory =
                new ParseQueryAdapter.QueryFactory<ParseObject>() {
                    public ParseQuery create() {
                        ParseQuery query = new ParseQuery("Services");
                        query.whereEqualTo("type", "PhoneService");
                        // query.orderByDescending("moneySpent");
                        return query;
                    }
                };

        ParseQueryAdapter<ParseObject> adapter = new ParseQueryAdapter<ParseObject>(this, factory);
        adapter.setTextKey("name");
        phoneList.setAdapter(adapter);

        findViewById(R.id.update_services).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                phoneName = (EditText) findViewById(R.id.service_text);
                phonePrice = (EditText) findViewById(R.id.price_text);
                ParseObject servicez = new ParseObject("Services");
                servicez.put("name", phoneName.getText().toString());
                servicez.put("price", Integer.parseInt(phonePrice.getText().toString()));
                servicez.put("type", "PhoneService");
                servicez.saveEventually();
            }
        });

        /*ParseQuery<ParseObject> query = ParseQuery.getQuery("Services");
        query.whereEqualTo("type", "CableService");
        query.findInBackground(new FindCallback<ParseObject>() {
            public void done(List<ParseObject> parseCableList, ParseException e) {
                if (e == null) {
                    Log.d("score", "Retrieved " + parseCableList.size() + " scores");
                } else {
                    Log.d("score", "Error: " + e.getMessage());
                }
            }
        }); */

        //ParseQueryAdapter<ParseObject> adapter = new ParseQueryAdapter<ParseObject>();

        //cableList.setAdapter();

        //Intent intent = getIntent();
        //final CustomerAccount customer = (CustomerAccount)intent.getSerializableExtra("currentCustomer");

        /*cableList = (ListView) findViewById(R.id.cable_list);


        String[] cableStrings = getResources().getStringArray(R.array.cable_services);
        for (int i = 0; i < cableStrings.length; i++)
        {
            if (cableStrings[i].equals(customer.getCable()))
            {
                cableList.setSelection(i);
            }
        }*/


        /*findViewById(R.id.updateServicesButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String internetService;
                String cableService;
                String phoneService;

                String temp = cableSpinner.getSelectedItem().toString();

                //Get cable service
                if(cableSpinner.getSelectedItem().toString().equals(getString(R.string.basic_cable))){
                    cableService = CableService.BASIC_CABLE;
                }
                else if(cableSpinner.getSelectedItem().toString().equals(getString(R.string.fox_news_only))){
                    cableService = CableService.FOX_NEWS_ONLY;
                }
                else{
                    cableService = CableService.NO_CABLE;
                }

                //Get internet
                if(internetSpinner.getSelectedItem().toString().equals(getString(R.string.basic_internet))){
                    internetService = InternetService.BASIC_INTERNET;
                }
                else if(internetSpinner.getSelectedItem().toString().equals(getString(R.string.korean_internet))){
                    internetService = InternetService.KOREAN_INTERNET;
                }
                else{
                    internetService = InternetService.NO_INTERNET;
                }

                //Get phone
                if(phoneSpinner.getSelectedItem().toString().equals(getString(R.string.basic_phone))){
                    phoneService = PhoneService.BASIC_PHONE;
                }
                else if(phoneSpinner.getSelectedItem().toString().equals(getString(R.string.spongebob_unlimited))){
                    phoneService = PhoneService.SPONGEBOB_UNLIMITED;
                }
                else{
                    phoneService = PhoneService.NO_PHONE;
                }

                customer.updateServices(internetService, cableService, phoneService);
                ParseQuery query = ParseUser.getQuery();
                query.whereEqualTo("username", customer.getUsername());
                query.findInBackground(new FindCallback<ParseUser>()
                {
                    @Override
                    public void done(List<ParseUser> parseUsers, ParseException e)
                    {
                        if (e == null)
                        {
                            ParseUser parseUser = parseUsers.get(0);
                            parseUser.put("internet", customer.getInternet());
                            parseUser.put("cable", customer.getCable());
                            parseUser.put("phone", customer.getPhone());

                            parseUser.saveInBackground(new SaveCallback()
                            {
                                @Override
                                public void done(ParseException e)
                                {
                                    if (e == null)
                                    {
                                        Toast.makeText(CustomerAccountActivity.this, getString(R.string.services_updated), Toast.LENGTH_LONG).show();
                                    }
                                    else
                                    {
                                        Toast.makeText(CustomerAccountActivity.this, "Unable to edit user information", Toast.LENGTH_LONG).show();
                                    }
                                }
                            });
                        }
                        else
                        {
                            Toast.makeText(CustomerAccountActivity.this, e.getMessage(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
            }
        });
    }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_customer_account, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}
